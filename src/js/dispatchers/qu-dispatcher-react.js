/************************
*---app-dispatcher.js---*
************************/

/*var Dispatcher = require('./dispatcher.js');
var merge  = require('react/lib/merge');

var AppDispatcher = merge(Dispatcher.prototype, {
  handleViewAction: function(action){
    console.log('action', action);
    this.dispatch({
      source: 'VIEW_ACTION',
      action: action
    })
  }
})

module.exports = AppDispatcher;*/



// Import Facebook's Dispatcher library
var Dispatcher = require('flux').Dispatcher;

// Create dispatcher instance
var AppDispatcher = new Dispatcher();

// Convenience method to handle dispatch requests
AppDispatcher.handleViewAction = function(action) {
	this.dispatch({
		source: 'VIEW_ACTION', // vs API_ACTION if it comes from server
		action: action
	});
}

module.exports = AppDispatcher;
