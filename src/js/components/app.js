/************************
*--------app.js---------*
************************/

/** @jsx React.DOM */
var React = require('react');
var Catalog = require('../components/qu-catalog-react.js');
var Cart = require('../components/qu-cart-react.js');

var APP = 
	React.createClass({
		render: function(){
			return (
				<div>
				<h1> Lets Shop </h1>
				<Catalog />
				<h1>Cart</h1>
				<Cart />
				</div>
			)
		}
	});

module.exports = APP;