//Used to bootstrap our quiqup application

//references to qjuery and bootstrap so that browserify is aware these javascript files are necessary
$ = jQuery = require('jquery');

//var App = console.log('Hello world from Browserify');

/** @jsx React.DOM */
var APP = require('./components/app');
var React = require('react');

React.render(
  <APP />,
  document.getElementById('app'));

//module.exports = App;